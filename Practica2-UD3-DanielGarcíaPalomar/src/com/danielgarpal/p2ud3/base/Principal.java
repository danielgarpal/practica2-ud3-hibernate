package com.danielgarpal.p2ud3.base;

import com.danielgarpal.p2ud3.gui.Controlador;
import com.danielgarpal.p2ud3.gui.Modelo;
import com.danielgarpal.p2ud3.gui.Vista;

public class Principal {
    //Clase main del programa, que crea una nueva vista, un nuevo modelo y un controlador con la
    //vista y el modelo
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
