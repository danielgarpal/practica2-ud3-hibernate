package com.danielgarpal.p2ud3.tablas;

import javax.persistence.*;
import java.util.Objects;

import static javax.persistence.GenerationType.AUTO;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "compra_producto", schema = "cafeteriahibernatemapeo", catalog = "")
public class Pedido {
    private int id;
    private int unidades;
    private Producto producto;
    private Compra compra;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "unidades")
    public int getUnidades() {
        return unidades;
    }

    public void setUnidades(int unidades) {
        this.unidades = unidades;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pedido pedido = (Pedido) o;
        return id == pedido.id &&
                unidades == pedido.unidades;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, unidades);
    }

    @ManyToOne
    @JoinColumn(name = "id_producto", referencedColumnName = "idproducto", nullable = false)
    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @ManyToOne
    @JoinColumn(name = "id_compra", referencedColumnName = "idcompra", nullable = false)
    public Compra getCompra() {
        return compra;
    }

    public void setCompra(Compra compra) {
        this.compra = compra;
    }
}
