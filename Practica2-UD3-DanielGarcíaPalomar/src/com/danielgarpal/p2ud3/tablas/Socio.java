package com.danielgarpal.p2ud3.tablas;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

import static javax.persistence.GenerationType.AUTO;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "socios", schema = "cafeteriahibernatemapeo", catalog = "")
public class Socio {
    private int dni;
    private Date fechaRegistro;
    private String nombreSocio;
    private List<Compra> compras;

    @Id
    @Column(name = "dni")
    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "fechaRegistro")
    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Basic
    @Column(name = "nombreSocio")
    public String getNombreSocio() {
        return nombreSocio;
    }

    public void setNombreSocio(String nombreSocio) {
        this.nombreSocio = nombreSocio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Socio socio = (Socio) o;
        return dni == socio.dni &&
                Objects.equals(fechaRegistro, socio.fechaRegistro) &&
                Objects.equals(nombreSocio, socio.nombreSocio);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dni, fechaRegistro, nombreSocio);
    }

    @OneToMany(mappedBy = "socio")
    public List<Compra> getCompras() {
        return compras;
    }

    public void setCompras(List<Compra> compras) {
        this.compras = compras;
    }

    @Override
    public String toString() {
        return
                "dni: " + dni +
                " - fecha registro: " + fechaRegistro +
                " - nombre: " + nombreSocio;
    }
}
