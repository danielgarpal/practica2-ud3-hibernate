package com.danielgarpal.p2ud3.tablas;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "productos", schema = "cafeteriahibernatemapeo", catalog = "")
public class Producto {
    private int idproducto;
    private String tipo;
    private String origen;
    private Date fechaEmpaquetado;
    private int intensidad;
    private float precio;
    private Proveedor proveedor;
    private List<Pedido> pedidos;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idproducto")
    public int getIdproducto() {
        return idproducto;
    }

    public void setIdproducto(int idproducto) {
        this.idproducto = idproducto;
    }

    @Basic
    @Column(name = "tipo")
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String nombre) {
        this.tipo = nombre;
    }

    @Basic
    @Column(name = "origen")
    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    @Basic
    @Column(name = "fechaEmpaquetado")
    public Date getFechaEmpaquetado() {
        return fechaEmpaquetado;
    }

    public void setFechaEmpaquetado(Date fechaEmpaquetado) {
        this.fechaEmpaquetado = fechaEmpaquetado;
    }

    @Basic
    @Column(name = "intensidad")
    public int getIntensidad() {
        return intensidad;
    }

    public void setIntensidad(int intensidad) {
        this.intensidad = intensidad;
    }

    @Basic
    @Column(name = "precio")
    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Producto producto = (Producto) o;
        return idproducto == producto.idproducto &&
                intensidad == producto.intensidad &&
                Float.compare(producto.precio, precio) == 0 &&
                Objects.equals(tipo, producto.tipo) &&
                Objects.equals(origen, producto.origen) &&
                Objects.equals(fechaEmpaquetado, producto.fechaEmpaquetado);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idproducto, tipo, origen, fechaEmpaquetado, intensidad, precio);
    }

    @ManyToOne
    @JoinColumn(name = "id_proveedor", referencedColumnName = "idProveedor", nullable = false)
    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    @OneToMany(mappedBy = "producto")
    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    @Override
    public String toString() {
        return
                idproducto +
                " -Nombre: " + tipo +
                " -Orig: " + origen +
                " -Empaquetado: " + fechaEmpaquetado +
                " -Intens: " + intensidad +
                " -€: " + precio +
                " -Prov: " + proveedor.getNombreProveedor();
    }
}
