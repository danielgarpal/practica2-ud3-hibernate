package com.danielgarpal.p2ud3.tablas;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "proveedores", schema = "cafeteriahibernatemapeo", catalog = "")
public class Proveedor {
    private int idProveedor;
    private String nombreProveedor;
    private String direccion;
    private String pais;
    private String telefono;
    private List<Producto> productos;



    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idProveedor")
    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    @Basic
    @Column(name = "nombreProveedor")
    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "pais")
    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Proveedor proveedor = (Proveedor) o;
        return idProveedor == proveedor.idProveedor &&
                Objects.equals(nombreProveedor, proveedor.nombreProveedor) &&
                Objects.equals(direccion, proveedor.direccion) &&
                Objects.equals(pais, proveedor.pais) &&
                Objects.equals(telefono, proveedor.telefono);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idProveedor, nombreProveedor, direccion, pais, telefono);
    }

    @OneToMany(mappedBy = "proveedor")
    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    @Override
    public String toString() {
        return
                idProveedor +
                "  - Nombre:  " + nombreProveedor  +
                "  - Dir:  " + direccion +
                "  - Pais:  " + pais +
                "  - Telef:  " + telefono;
    }
}
