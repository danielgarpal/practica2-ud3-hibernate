package com.danielgarpal.p2ud3.tablas;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "compras", schema = "cafeteriahibernatemapeo", catalog = "")
public class Compra {
    private int idcompra;
    private byte descuentoSocio;
    private float total;
    private Date fechaVenta;
    private List<Pedido> pedidos;
    private Socio socio;

    @Id
    // @GeneratedValue(strategy = IDENTITY)
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idcompra")

    public int getIdcompra() {
        return idcompra;
    }

    public void setIdcompra(int idcompra) {
        this.idcompra = idcompra;
    }

    @Basic
    @Column(name = "descuentoSocio")
    public byte getDescuentoSocio() {
        return descuentoSocio;
    }

    public void setDescuentoSocio(byte descuentoSocio) {
        this.descuentoSocio = descuentoSocio;
    }

    @Basic
    @Column(name = "total")
    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    @Basic
    @Column(name = "fechaVenta")
    public Date getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(Date fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Compra compra = (Compra) o;
        return idcompra == compra.idcompra &&
                descuentoSocio == compra.descuentoSocio &&
                Float.compare(compra.total, total) == 0 &&
                Objects.equals(fechaVenta, compra.fechaVenta);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idcompra, descuentoSocio, total, fechaVenta);
    }

    @OneToMany(mappedBy = "compra")
    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    @ManyToOne
    @JoinColumn(name = "id_socio", referencedColumnName = "dni", nullable = false)
    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }



    @Override
    public String toString() {
        return  idcompra +
                " - Descuento: " + descuentoSocio +
                " - total: " + total +
                " - fechaVenta: " + fechaVenta +
                " - socio: " + socio;
    }
}
