package com.danielgarpal.p2ud3.gui;

import com.danielgarpal.p2ud3.tablas.*;
import com.danielgarpal.p2ud3.util.Util;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import javax.xml.bind.Marshaller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.ArrayList;

public class Controlador implements ActionListener {
    private Modelo modelo;
    private Vista vista;
    boolean refrescado;

    //Constructor de la clase
    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;


        addActionListeners(this);
        addItemListeners(this);


    }


    //Método que refresca todas las tablas y vacía todos los campos
    private void refrescarTodo() {
        refrescarProductos();
        refrescarCompras();
        refrescarSocios();
        refrescarProveedores();
        refrescado = false;

        borrarCamposCompras();
        borrarCamposSocios();
        borrarCamposProductos();
        borrarCamposProveedores();

    }



    //Añade los listeners a los botones
    private void addActionListeners(ActionListener listener) {
        vista.proveedorAniadir.addActionListener(listener);
        vista.proveedorAniadir.setActionCommand("aniadirProveedor");
        vista.proveedorModificar.addActionListener(listener);
        vista.proveedorModificar.setActionCommand("modificarProveedor");
        vista.proveedorElminiar.addActionListener(listener);
        vista.proveedorElminiar.setActionCommand("eliminarProveedor");
        vista.refrescarProv.addActionListener(listener);
        vista.refrescarProv.setActionCommand("refProv");

        vista.productoAniadir.addActionListener(listener);
        vista.productoAniadir.setActionCommand("aniadirProducto");
        vista.productoModificar.addActionListener(listener);
        vista.productoModificar.setActionCommand("modificarProducto");
        vista.productoEliminar.addActionListener(listener);
        vista.productoEliminar.setActionCommand("eliminarProducto");
        vista.refrescarProd.addActionListener(listener);
        vista.refrescarProd.setActionCommand("refProd");

        vista.compraAniadir.addActionListener(listener);
        vista.compraAniadir.setActionCommand("aniadirCompra");
        vista.compraModificar.addActionListener(listener);
        vista.compraModificar.setActionCommand("modificarCompra");
        vista.compraEliminar.addActionListener(listener);
        vista.compraEliminar.setActionCommand("eliminarCompra");
        vista.refrescarCompra.addActionListener(listener);
        vista.refrescarCompra.setActionCommand("refCompra");

        vista.socioAniadir.addActionListener(listener);
        vista.socioAniadir.setActionCommand("aniadirSocio");
        vista.socioModificar.addActionListener(listener);
        vista.socioModificar.setActionCommand("modificarSocio");
        vista.socioEliminar.addActionListener(listener);
        vista.socioEliminar.setActionCommand("eliminarSocio");
        vista.refrescarSoc.addActionListener(listener);
        vista.refrescarSoc.setActionCommand("refSoc");
        vista.socioComprasDelSocio.addActionListener(listener);
        vista.socioComprasDelSocio.setActionCommand("comprasSocio");


    }
    //Añade los listeners a los items del menu bar
    private void addItemListeners(ActionListener listener) {
        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.itemDesconectar.addActionListener(listener);
        vista.itemRefrescar.addActionListener(listener);


    }

    //Método que se ejecuta al ser pulsado un botón
    @Override
    public void actionPerformed(ActionEvent e) {
         String command = e.getActionCommand();

         switch (command){

             case "Conectar":
                 modelo.conectar();
                 //refrescarTodo();
                 break;
             //Cierra la conexión
             case "Desconectar":modelo.desconectar();
                 break;
             //Cierra el programa
             case "Salir": System.exit(0);
                 break;
                 //Refresca todas las tabla
             case "Refrescar":
                 refrescarTodo();
                 break;
                 //Método que añade un proveedor
             case "aniadirProveedor":
                 try {
                     //Comprueba que todos los campos estén rellenados
                     if (comprobarProveedorVacio()) {
                         Util.showErrorAlert("Rellena todos los campos");
                         vista.proveedorTablaProveedor.clearSelection();
                         //Comprueba que no existe ese nombre
                     } else if (modelo.proveedorExiste(vista.proveedorNombre.getText())) {
                         Util.showErrorAlert("Ese nombre ya existe.\nIntroduce un proveedor diferente");
                         vista.proveedorTablaProveedor.clearSelection();
                     } else {
                         //Crea un nuevo proeedore y le mete los datos
                         Proveedor proveedor = new Proveedor();
                            proveedor.setNombreProveedor(vista.proveedorNombre.getText());
                            proveedor.setDireccion(vista.proveedorDireccion.getText());
                            proveedor.setPais(vista.proveedorPais.getText());
                            proveedor.setTelefono(vista.proveedorTelefono.getText());
                            //Envía el proveedor al método que lo inserta en la base de datos
                         modelo.insertarProveedor(proveedor);

                         //Refresca la lista y el combo box
                         refrescarProveedores();
                     }
                 } catch (NumberFormatException nfe) {
                     Util.showErrorAlert("Introduce números en los campos que lo requieren");
                     vista.proveedorTablaProveedor.clearSelection();
                 }
                 //Vacía los campos
                 borrarCamposProveedores();
                 break;
            /*
                Comprueba que los campos de producto no están vacios e introduce en el método insertar producto los datos del proveedor
                 */
             case "aniadirProducto":
                 try {
                     //Comprueba que los campos no estén vacíos
                     if (comprobarProductoVacio()) {
                         Util.showErrorAlert("Rellena todos los campos");
                         vista.productoTablaProducto.clearSelection();
                     } else {
                         //Creo un nuevo Producto y le introduzco los datos
                         Producto producto = new Producto();
                         producto.setTipo(vista.productoNombre.getText());
                         producto.setOrigen(vista.productoorigen.getText());
                         producto.setFechaEmpaquetado(Date.valueOf(vista.productoFecha.getDate()));//Timestamp.valueOf(vista.productoFecha.getDateTimePermissive())
                         producto.setIntensidad(Integer.parseInt(vista.productoIntensidad.getText()));
                         producto.setPrecio(Float.parseFloat(vista.productoPrecio.getText()));
                         producto.setProveedor((Proveedor) vista.productoProveedorComboBox.getSelectedItem());
                         //Envía el producto al método que lo introduce en la base de datos
                         modelo.insertarProducto(producto);
                        //Refresca la lista y el combo box
                         refrescarProductos();
                     }
                 } catch (NumberFormatException nfe) {
                     Util.showErrorAlert("Introduce números en los campos que lo requieren");
                     vista.proveedorTablaProveedor.clearSelection();
                 }
                //Vacía los campos
                 borrarCamposProductos();
                 break;
            /*
                Comprueba que los campos de compra no están vacios
                e introduce en el método insertar compra los datos del proveedor
                 */
             case "aniadirCompra":
                 try {
                     if (comprobarCompraVacio()) {
                         Util.showErrorAlert("Rellena todos los campos");
                         vista.compratablacompras.clearSelection();
                     } else {
                         byte descuento =0;
                         //Si el checkbox está marcado introduce 1 en la base de datos, si no es 0
                         if (vista.compraSiDescuento.isSelected()){descuento=1;}
                         //Creo una nueva compra y le meto los datos
                         Compra compra = new Compra();
                         //Creo un nuevo pedido(Tabla intermedia)
                         Pedido unPedido = new Pedido();
                         //Creo una lista de pedidos(ArrayList)
                         ArrayList<Pedido> pedido=new ArrayList<Pedido>();
                         //Añade la compra y el producto al pedido
                         unPedido.setCompra(compra);
                         unPedido.setProducto((Producto) vista.compraProductoComboBox.getSelectedItem());
                         unPedido.setUnidades(Integer.parseInt(vista.compraCantidad.getText()));
                         pedido.add(unPedido);
                         compra.setDescuentoSocio(descuento);
                         compra.setFechaVenta(Date.valueOf(vista.compraFecha.getDate()));
                         compra.setTotal(Float.parseFloat(vista.compraTotal.getText()));
                         compra.setSocio((Socio) vista.compraSocioComboBox.getSelectedItem());


                         //Inserta la compra y el pedido en la base de datos
                         modelo.insertarCompra(compra);
                         modelo.insertarPedido(unPedido);
                        //Refresca los datos
                         refrescarCompras();
                     }
                 } catch (NumberFormatException nfe) {
                     Util.showErrorAlert("Introduce números en los campos que lo requieren con el formato '0' o '0.0'");
                     vista.compratablacompras.clearSelection();
                 }
                    //Vacía los campos
                     borrarCamposCompras();
                     break;
                     //Método que crea un socio
             case "aniadirSocio":
                 try {
                     //Comprueba que los datos no estén vacíos
                     if (comprobarSocioVacio()) {
                         Util.showErrorAlert("Rellena todos los campos");
                         vista.sociotablasocios.clearSelection();
                     } else {
                         //Creo un nuevo Socio y añade los datos
                         Socio socio = new Socio();
                         socio.setDni(Integer.parseInt(vista.sociodni.getText()));
                         socio.setFechaRegistro(Date.valueOf(vista.sociofecha.getDate()));
                         socio.setNombreSocio(vista.socionombre.getText());
                         modelo.insertarSocio(socio);
                         refrescarSocios();
                     }
                 } catch (NumberFormatException nfe) {
                     Util.showErrorAlert("Introduce números en los campos que lo requieren");
                     vista.sociotablasocios.clearSelection();
                 } catch (Exception e1){
                     Util.showErrorAlert("Ha habido un error");
                     vista.sociotablasocios.clearSelection();
                 }

                 borrarCamposSocios();
                 break;
            //Elimina un proveedor
             case "eliminarProveedor":
                 //Creo un nuevo proveedor y le introdzco el seleccionado en la lista
                 Proveedor provBorrado = (Proveedor) vista.proveedorTablaProveedor.getSelectedValue();
                 boolean e2=false;
                 //Comprueba que no borres un proveedor relacionado en un producto
                 for (Producto p : modelo.consultarProducto()) {
                     if (p.getProveedor().getIdProveedor() == provBorrado.getIdProveedor()){
                         Util.showErrorAlert("No puedes borrar este producto. Tiene datos relacionados");
                         e2=true;
                     }

                 }if (e2==false){modelo.borrarProveedor(provBorrado);}

                 break;
                 //Elimina un producto
             case "eliminarProducto":
                 //Crea un producto con el seleccionado en la base de datos
                 Producto prodBorrado = (Producto) vista.productoTablaProducto.getSelectedValue();

                 boolean errorproducto=false;
                //Lo manda al método de borrar
                 modelo.borrarProducto(prodBorrado);
                 break;
                 //Elimina una compra
             case "eliminarCompra":
                 //Crea un objeto con la compra seleccionada y lo manda borrar
                 Compra compBorrado = (Compra) vista.compratablacompras.getSelectedValue();
                 modelo.borrarCompra(compBorrado);
                 break;
             //Crea un objeto con el socio seleccionada y lo manda borrar
             case "eliminarSocio":
                 Socio socBorrado = (Socio) vista.sociotablasocios.getSelectedValue();
                 modelo.borrarSocio(socBorrado);
                 break;
                 //Modifica un proveedor
             case "modificarProveedor":

                 try {
                     //Comprueba que los campos no estén vacíos
                     if (comprobarProveedorVacio()) {
                         Util.showErrorAlert("Rellena todos los campos");
                         vista.proveedorTablaProveedor.clearSelection();
                     } else {
                         //Creo un nuevo proveedor con el de la lista y le meto nuevos datos
                         Proveedor provModificado =(Proveedor) vista.proveedorTablaProveedor.getSelectedValue();
                         provModificado.setNombreProveedor(vista.proveedorNombre.getText());
                         provModificado.setDireccion(vista.proveedorDireccion.getText());
                         provModificado.setPais(vista.proveedorPais.getText());
                         provModificado.setTelefono(vista.proveedorTelefono.getText());
                         //Lo mando al método de modificar
                         modelo.modificarProveedor(provModificado);

                         refrescarProveedores();
                     }
                 } catch (NumberFormatException nfe) {
                     Util.showErrorAlert("Introduce números en los campos que lo requieren");
                     vista.proveedorTablaProveedor.clearSelection();
                 }
                 borrarCamposProveedores();

                 break;
                 //Modifica un producto
             case "modificarProducto":
                 try {
                     if (comprobarProductoVacio()) {
                         Util.showErrorAlert("Rellena todos los campos");
                         vista.productoTablaProducto.clearSelection();
                     } else {
                         //Creo un nuevo producto con el de la lista y le meto nuevos datos
                         Producto prodModificado = (Producto) vista.productoTablaProducto.getSelectedValue();
                         prodModificado.setTipo(vista.productoNombre.getText());
                         prodModificado.setOrigen(vista.productoorigen.getText());
                         prodModificado.setFechaEmpaquetado(Date.valueOf(vista.productoFecha.getDate()));//Timestamp.valueOf(vista.productoFecha.getDateTimePermissive())
                         prodModificado.setIntensidad(Integer.parseInt(vista.productoIntensidad.getText()));
                         prodModificado.setPrecio(Float.parseFloat(vista.productoPrecio.getText()));
                         prodModificado.setProveedor((Proveedor) vista.productoProveedorComboBox.getSelectedItem());
                         //Lo mando al método de modificar
                         modelo.modificarProducto(prodModificado);

                         refrescarProductos();
                     }
                 } catch (NumberFormatException nfe) {
                     Util.showErrorAlert("Introduce números en los campos que lo requieren");
                     vista.proveedorTablaProveedor.clearSelection();
                 }

                 borrarCamposProductos();

                 break;
                 //Modificar compra
             case "modificarCompra":
                 try {
                     if (comprobarCompraVacio()) {
                         Util.showErrorAlert("Rellena todos los campos");
                         vista.compratablacompras.clearSelection();
                     } else {
                         byte descuento =0;
                         if (vista.compraSiDescuento.isSelected()){descuento=1;}
                         //Creo una nueva compra con el de la lista y le meto nuevos datos
                         Compra compraMod = (Compra) vista.compratablacompras.getSelectedValue();
                         //Creo un nuevo pedido para registrar la compra
                         Pedido unPedido = new Pedido();
                         ArrayList<Pedido> pedido=new ArrayList<Pedido>();
                         unPedido.setCompra(compraMod);
                         unPedido.setProducto((Producto) vista.compraProductoComboBox.getSelectedItem());
                         unPedido.setUnidades(Integer.parseInt(vista.compraCantidad.getText()));
                         pedido.add(unPedido);
                         compraMod.setDescuentoSocio(descuento);
                         compraMod.setFechaVenta(Date.valueOf(vista.compraFecha.getDate()));
                         compraMod.setTotal(Float.parseFloat(vista.compraTotal.getText()));
                         compraMod.setSocio((Socio) vista.compraSocioComboBox.getSelectedItem());

                        //Añado la compra y el pedido a la base de datos
                         modelo.modificarCompra(compraMod);
                         modelo.insertarPedido(unPedido);

                         refrescarCompras();
                     }
                 } catch (NumberFormatException nfe) {
                     Util.showErrorAlert("Introduce números en los campos que lo requieren con el formato '0' o '0.0'");
                     vista.compratablacompras.clearSelection();
                 }

                 borrarCamposCompras();
                 break;
                 //Modifica un socio
             case "modificarSocio":
                 try {
                     if (comprobarSocioVacio()) {
                         Util.showErrorAlert("Rellena todos los campos");
                         vista.sociotablasocios.clearSelection();
                     } else {
                         //Creo un nuevo socio con el de la lista y le meto nuevos datos
                         Socio socioModificado = (Socio) vista.sociotablasocios.getSelectedValue();


                         socioModificado.setDni(Integer.parseInt(vista.sociodni.getText()));
                         socioModificado.setFechaRegistro(Date.valueOf(vista.sociofecha.getDate()));
                         socioModificado.setNombreSocio(vista.socionombre.getText());
                         //Lo mando al método de modificar
                         modelo.modificarSocio(socioModificado);
                         //Refresco los datos
                         refrescarSocios();
                     }
                 } catch (NumberFormatException nfe) {
                     Util.showErrorAlert("Introduce números en los campos que lo requieren");
                     vista.sociotablasocios.clearSelection();
                 } catch (Exception e1){
                     Util.showErrorAlert("Ha habido un error");
                     vista.sociotablasocios.clearSelection();
                 }

                 borrarCamposSocios();
                 break;
                 //Métodos que refrescan los datos individualmente de cada tabla
             case "refProv":
                 refrescarProveedores();
                 break;
             case "refProd":
                 refrescarProductos();
                 break;
             case "refCompra":
                 refrescarCompras();
                 break;
             case "refSoc":
                 refrescarSocios();
                 break;
                 //Método que muestra las compras relacionadas con un socio
             case "comprasSocio":
                 //Crea un socio con el seleccionado en la lista
                 Socio socioSelec = (Socio) vista.sociotablasocios.getSelectedValue();
                 //Limpia la lista de comprasSocios
                 vista.dlmComprasSocios.clear();
                 //Recorre la lista de compras
                 for (Compra compra :modelo.consultarCompra()) {
                     //Cuando una compra tenga el dni del socio seleccionado la añadirá a la lista
                     if (socioSelec.getDni()==compra.getSocio().getDni()){
                         vista.dlmComprasSocios.addElement(compra);

                     }

                 }
                 break;


         }

    }

    //Método que llama a refrescar la lista de proveedores con un ArrayList actualizado
    //y actualiza el combobox de proveedores
    private void refrescarProveedores() {
        //listar los proveedores en el dlm
        listarProveedores(modelo.consultarProveedor());
        //Limpiar combo box proveedores
        vista.productoProveedorComboBox.removeAllItems();
        //Relleno el combo box con el array list de proveedores
        for(Proveedor proveedor : modelo.consultarProveedor()){
            //Relleno el item con proveedor
            vista.productoProveedorComboBox.addItem(proveedor);


        }

    }
    //Método que rellena la lista de proveedores con los datos del ArrayList de proveedores
    private void listarProveedores(ArrayList<Proveedor> lista){
        vista.dlmProveedores.clear();
        for(Proveedor proveedor : lista){
            vista.dlmProveedores.addElement(proveedor);
        }
    }
    //Método que llama a refrescar la lista de scios con un ArrayList actualizado y
    //actualiza el combobox de socios
    private void refrescarSocios() {
        //listar los socios en el dlm
        listarSocios(modelo.consultarSocio());
        //Limpiar combo box socio
        vista.compraSocioComboBox.removeAllItems();

        //Relleno el combo box con el array list de socios
        for(Socio socio : modelo.consultarSocio()){
            //Relleno el item con el dni del socio y el nombre
            vista.compraSocioComboBox.addItem(socio);

        }
    }
    //Método que rellena la lista de socios con los datos del ArrayList de socios
    private void listarSocios(ArrayList<Socio> lista){
        vista.dlmSocios.clear();
        for(Socio socio : lista){
            vista.dlmSocios.addElement(socio);
        }
    }
    //Método que llama a refrescar la lista de productos con un ArrayList actualizado y
    //actualiza el combobox de productos
    private void refrescarProductos() {
        //Listar los productos en la vista
        listarProductos(modelo.consultarProducto());

        //Limpiar combobox producto
        vista.compraProductoComboBox.removeAllItems();
        for (Producto producto: modelo.consultarProducto()) {
            vista.compraProductoComboBox.addItem(producto);
        }


    }
    //Método que rellena la lista de socios con los datos del ArrayList de socios
    private void listarProductos(ArrayList<Producto> lista){

        vista.dlmProductos.clear();

        for(Producto producto : lista){
            vista.dlmProductos.addElement(producto);
        }
    }
    //Método que llama a refrescar la lista de compras con un ArrayList actualizado
    private void refrescarCompras() {
        //listar compras en la vista
        listarCompras(modelo.consultarCompra());
    }
    //Método que rellena la lista de compras con los datos del ArrayList de compras
    private void listarCompras(ArrayList<Compra> lista){
        vista.dlmCompras.clear();
        for(Compra compra : lista){
            vista.dlmCompras.addElement(compra);
        }
    }





    //BORRAR CAMPOS
    //Método que vacía los campos de la pestaña productos
    private void borrarCamposProductos() {
        vista.productoNombre.setText("");
        vista.productoorigen.setText("");
        vista.productoFecha.setText("");
        vista.productoIntensidad.setText("");
        vista.productoPrecio.setText("");
        vista.productoProveedorComboBox.setSelectedIndex(-1);

    }
    //Método que vacía los campos de la pestaña socios
    private void borrarCamposSocios(){
        vista.sociodni.setText("");
        vista.sociofecha.setText("");
        vista.socionombre.setText("");


    }
    //Método que vacía los campos de la pestaña compras
    private void borrarCamposCompras() {
        vista.compraSiDescuento.setSelected(false);
        vista.compraFecha.setText("");
        vista.compraSocioComboBox.setSelectedIndex(-1);
        vista.compraProductoComboBox.setSelectedIndex(-1);
        vista.compraTotal.setText("");
        vista.compraCantidad.setText("");

    }
    //Método que vacía los campos de la pestaña proveedores
    private void borrarCamposProveedores() {
        vista.proveedorNombre.setText("");
        vista.proveedorDireccion.setText("");
        vista.proveedorPais.setText("");
        vista.proveedorTelefono.setText("");
    }



    //COMPROVAR CAMPOS VACÍOS
    //Método que comprueba si todos los campos estan vacíos en la pestaña producto
    private boolean comprobarProductoVacio() {
if(vista.productoNombre.getText().isEmpty() || vista.productoorigen.getText().isEmpty() ||
vista.productoFecha.getText().isEmpty() || vista.productoIntensidad.getText().isEmpty() ||
vista.productoPrecio.getText().isEmpty() || vista.productoProveedorComboBox.getSelectedIndex() == -1){
    return true;
}else{
    return false;
}
    }
    //Método que comprueba si todos los campos estan vacíos en la pestaña proveedor
    private boolean comprobarProveedorVacio() {
        if(vista.proveedorNombre.getText().isEmpty() || vista.proveedorDireccion.getText().isEmpty()  ||
                vista.proveedorPais.getText().isEmpty()   ||  vista.proveedorTelefono.getText().isEmpty()){
            return true;
        }else{
            return false;
        }
    }
    //Método que comprueba si todos los campos estan vacíos en la pestaña compra
    private boolean comprobarCompraVacio(){
        if(vista.compraTotal.getText().isEmpty() || vista.compraFecha.getText().isEmpty() ||
                vista.compraSocioComboBox.getSelectedIndex() == -1 || vista.compraProductoComboBox.getSelectedIndex() == -1){
            return true;
        }else{
            return false;
        }
    }
    //Método que comprueba si todos los campos estan vacíos en la pestaña socio
    private boolean comprobarSocioVacio(){
        if(vista.sociodni.getText().isEmpty() || vista.sociofecha.getText().isEmpty() ||
        vista.socionombre.getText().isEmpty()){
            return true;
        }else{
            return false;
        }
    }


    }
