package com.danielgarpal.p2ud3.gui;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

public class Vista extends JFrame{
    private final static String TITULOFRAME = "Aplicación Hibernate";
    public JPanel panel1;
    public JTabbedPane tabbedPane1;
    public JTextField proveedorNombre;
    public JTextField proveedorDireccion;
    public JTextField proveedorPais;
    public JTextField proveedorTelefono;
    public JList proveedorTablaProveedor;
    public JButton proveedorElminiar;
    public JButton proveedorAniadir;
    public JButton proveedorModificar;
    public JTextField productoNombre;
    public JTextField productoorigen;
    public JTextField productoIntensidad;
    public JTextField productoPrecio;
    public JComboBox productoProveedorComboBox;
    public JTextField compraTotal;
    public JComboBox compraSocioComboBox;
    public JTextField sociodni;
    public JTextField socionombre;
    public JButton socioAniadir;
    public JButton socioModificar;
    public JButton socioEliminar;
    public JButton compraAniadir;
    public JButton compraModificar;
    public JButton compraEliminar;
    public JButton productoAniadir;
    public JButton productoModificar;
    public JButton productoEliminar;
    public JList productoTablaProducto;
    public JList compratablacompras;
    public JCheckBox compraSiDescuento;
    public JList sociotablasocios;
    public JList sociotablacompras;
    public DatePicker sociofecha;
    public DatePicker compraFecha;
    public DatePicker productoFecha;
    public JComboBox compraProductoComboBox;
    public JButton refrescarProv;
    public JButton refrescarProd;
    public JButton refrescarCompra;
    public JButton refrescarSoc;
    public JTextField compraCantidad;
    public JButton socioComprasDelSocio;


    //Menu
    public JMenuItem itemConectar;
    public JMenuItem itemDesconectar;
    public JMenuItem itemSalir;
    public JMenuItem itemRefrescar;

    //DEFAULT TABLE MODEL
    public DefaultListModel dlmProveedores;
    public DefaultListModel dlmProductos;
    public DefaultListModel dlmCompras;
    public DefaultListModel dlmSocios;
    public DefaultListModel dlmComprasSocios;


    //Constructor de la clase vista, que utiliza el título de la aplicación y se dirige al método que inicia el panel
    public Vista() {
        super(TITULOFRAME);
        initFrame();
    }

    private void initFrame() {
             this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth() + 200, this.getHeight() + 100));
        //this.setLocationRelativeTo(this);
        this.setLocationRelativeTo(null);

        setMenu();
        setListModels();


    }


    /*
    Método que crea un menú barra para guardar opciones como modificar los datos de conexión o salir
     */
    private void setMenu() {
        //Creo nueva barra de menú
        JMenuBar menuBar = new JMenuBar();

        //Creo un menú y le pongo título
        JMenu menu = new JMenu("Menu");


        //Items del menú y actiion commands

        //Opciones
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");

        //Desconectar
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");

        // Salir
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        //Refrescar todo
        itemRefrescar = new JMenuItem("Refrescar todo");
        itemRefrescar.setActionCommand("Refrescar");

        //Añadir items al menú
        menu.add(itemConectar);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        menu.add(itemRefrescar);

        //Añadir el menú a la barra de menú
        menuBar.add(menu);

        //Central en horizontal
        menuBar.add(Box.createHorizontalGlue());

        //Añadir la barra de menú a la vista
        this.setJMenuBar(menuBar);


    }
    private void setListModels() {
        this.dlmCompras=new DefaultListModel();
        this.compratablacompras.setModel(dlmCompras);
        this.dlmProductos=new DefaultListModel();
        this.productoTablaProducto.setModel(dlmProductos);
        this.dlmProveedores=new DefaultListModel();
        this.proveedorTablaProveedor.setModel(dlmProveedores);
        this.dlmSocios = new DefaultListModel();
        this.sociotablasocios.setModel(dlmSocios);
        this.dlmComprasSocios = new DefaultListModel();
        this.sociotablacompras.setModel(dlmComprasSocios);
    }



}
