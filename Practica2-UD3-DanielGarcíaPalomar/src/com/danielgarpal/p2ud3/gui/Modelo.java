package com.danielgarpal.p2ud3.gui;

import com.danielgarpal.p2ud3.tablas.*;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Modelo {
    SessionFactory sessionFactory;

    private String ip;
    private String user;
    private String password;
    private String adminpassword;
    private String puerto;

    private Connection conexion;

    public Modelo() {
       // getPropValues();

    }
    // GETTERS
    public String getIp() { return ip; }

    public String getUser() { return user; }

    public String getPassword() { return password; }

    public String getAdminPassword() { return adminpassword; }




    /*
    Método conectar, que intenta realizar la conexión con la base de datos a través de unos parámetros guardados en un fichero
     */
    public void conectar() {
        Configuration configuracion = new Configuration();
        //Cargo el fichero Hibernate.cfg.xml
        configuracion.configure("hibernate.cfg.xml");

        //Indico la clase mapeada con anotaciones
        configuracion.addAnnotatedClass(Compra.class);
        configuracion.addAnnotatedClass(Pedido.class);
        configuracion.addAnnotatedClass(Producto.class);
        configuracion.addAnnotatedClass(Proveedor.class);
        configuracion.addAnnotatedClass(Socio.class);


        //Creamos un objeto ServiceRegistry a partir de los parámetros de configuración
        //Esta clase se usa para gestionar y proveer de acceso a servicios
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();

        //finalmente creamos un objeto sessionfactory a partir de la configuracion y del registro de servicios
        sessionFactory = configuracion.buildSessionFactory(ssr);

    }


    //Método que cierra la conexión
    public void desconectar() {
        //Cierro la factoria de sessiones
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();

    }
    //Método que guarda en las variables los parámetros de conexión del fichero properties
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";
            inputStream = new FileInputStream(propFileName);
            prop.load(inputStream);
            //Guarda los valores en las variables
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminpassword = prop.getProperty("admin");
            puerto=prop.getProperty("puerto");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }
    public void setPropValues(String ip, String tfpuerto, String user, String ps, String psa) {
        this.ip=ip;
        this.user=user;
        this.password=ps;
        this.adminpassword=psa;
        this.puerto=tfpuerto;

        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("puerto",tfpuerto);
            prop.setProperty("user", user);
            prop.setProperty("pass", ps);
            prop.setProperty("admin", psa);
            OutputStream out = null;

            out = new FileOutputStream("config.properties");
            prop.store(out, null);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    //Inserta en la base de datos
    public void insertarProducto(Producto producto) {
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(producto);
        sesion.getTransaction().commit();

        sesion.close();

    }
    //Inserta en la base de datos
        public void insertarCompra(Compra compra) {
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(compra);
        sesion.getTransaction().commit();

        sesion.close();

    }
    //Inserta en la base de datos
    public void insertarSocio(Socio socio) {
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(socio);
        sesion.getTransaction().commit();

        sesion.close();

    }
    //Inserta en la base de datos
    public void insertarProveedor(Proveedor proveedor) {
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(proveedor);
        sesion.getTransaction().commit();

        sesion.close();

    }
    //Inserta en la base de datos
    public void insertarPedido(Pedido pedido) {
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(pedido);
        sesion.getTransaction().commit();

        sesion.close();
    }

    //Metodo que comprueba si existe un proveedor en la base de datos
    public boolean proveedorExiste(String nombreRecibido) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.
                createQuery("FROM Proveedor p WHERE p.nombreProveedor = :nombre");
        query.setParameter("nombre", nombreRecibido);
        Proveedor unProveedor = (Proveedor) query.uniqueResult();

        sesion.close();

        if(unProveedor==null){
            return false;
        }else {
            return true;
        }

    }
    //Metodo que comprueba si existe un socio en la base de datos
    public boolean socioExiste(String dni) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.
                createQuery("FROM Socio s WHERE s.dni = :dni");
        query.setParameter("dni", dni);
        Socio unSocio = (Socio) query.uniqueResult();

        sesion.close();

        if(unSocio==null){
            return false;
        }else {
            return true;
        }

    }

    //Método que devuelve una lista de la clase
    public ArrayList<Proveedor> consultarProveedor() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Proveedor ");
        ArrayList<Proveedor> lista = (ArrayList<Proveedor>)query.getResultList();
        sesion.close();
        return lista;

    }
    //Método que devuelve una lista de la clase
    public ArrayList<Socio> consultarSocio() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Socio ");
        ArrayList<Socio> lista = (ArrayList<Socio>)query.getResultList();
        sesion.close();
        return lista;
    }
    //Método que devuelve una lista de la clase
    public ArrayList<Producto> consultarProducto() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Producto ");
        ArrayList<Producto> lista = (ArrayList<Producto>)query.getResultList();
        sesion.close();
        return lista;
    }
    //Método que devuelve una lista de la clase
    public ArrayList<Compra> consultarCompra() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Compra ");
        ArrayList<Compra> lista = (ArrayList<Compra>)query.getResultList();
        sesion.close();
        return lista;
    }
    //Método que devuelve una lista de la clase
    public ArrayList<Pedido> consultarPedidos(Compra compraPedido) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Pedido WHERE compra = :compraPedido");
        ArrayList<Pedido> lista = (ArrayList<Pedido>)query.getResultList();
        sesion.close();
        return lista;
    }
    //Método que borra en la base de datos
    public void borrarProveedor(Proveedor provBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(provBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }
    //Método que borra en la base de datos
    public void borrarProducto(Producto prodBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(prodBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }
    //Método que borra en la base de datos
    public void borrarCompra(Compra compBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(compBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }
    //Método que borra en la base de datos
    public void borrarSocio(Socio socBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(socBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }
    //Método que modifica los datos
    public void modificarProveedor(Proveedor proveedor) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(proveedor);
        sesion.getTransaction().commit();
        sesion.close();
    }
    //Método que modifica los datos
    public void modificarProducto(Producto producto) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(producto);
        sesion.getTransaction().commit();
        sesion.close();
    }
    //Método que modifica los datos
    public void modificarCompra(Compra compra) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(compra);
        sesion.getTransaction().commit();
        sesion.close();
    }
    //Método que modifica los datos
    public void modificarSocio(Socio socio) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(socio);
        sesion.getTransaction().commit();
        sesion.close();
    }





}
