


CREATE DATABASE IF NOT EXISTS CafeTeriaHibernateMapeo;
--
USE CafeTeriaHibernateMapeo;
--
CREATE TABLE IF NOT EXISTS proveedores(
idProveedor INT auto_increment PRIMARY KEY,
nombreProveedor VARCHAR(50) NOT NULL,
direccion VARCHAR(150) NOT NULL,
pais VARCHAR(50) NOT NULL,
telefono VARCHAR(9) NOT NULL
);
--

CREATE TABLE IF NOT EXISTS productos (
idproducto INT auto_increment PRIMARY KEY,
tipo VARCHAR(10) NOT NULL,
origen VARCHAR(20) NOT NULL,
fechaEmpaquetado DATE NOT NULL,
intensidad INT NOT NULL,
precio FLOAT NOT NULL,
id_proveedor INT UNSIGNED NOT NULL REFERENCES proveedores
);
--

CREATE TABLE IF NOT EXISTS compras(
idcompra INT auto_increment PRIMARY KEY,
descuentoSocio boolean NOT NULL,
total FLOAT NOT NULL,
fechaVenta DATE NOT NULL,
id_socio INT UNSIGNED NOT NULL REFERENCES socios
);
--

CREATE TABLE IF NOT EXISTS socios(
dni INT PRIMARY KEY,
fechaRegistro DATE NOT NULL,
nombreSocio VARCHAR(50) NOT NULL
);


CREATE TABLE IF NOT EXISTS compra_producto(
id_compra INT UNSIGNED REFERENCES compras,
id_producto INT UNSIGNED REFERENCES productos,
unidades INT NOT NULL,
PRIMARY KEY (id_compra, id_producto)
);

